# Chez Scheme specs testing library.

This library is a small imitation of a Ruby specs testing framework (or Node.js Jest `describe/it` facility)
I know about the existence of SRFIs, but for me their implicit management of testing state feels a bit uncomfortable.

In this library tests are just functions returning special type of result, and test sets are function returning lists of test functions. 
To run a suite I run the returned functions without reliance on some invisible state somewhere.

## Usage

Macro `define-specs` is the main test definition facility. It provides 3 local macros: `describe`, `context` and `it` which roughly correspond to their Ruby equivalents. `describe` and `context` are are synonyms.

Assertions implemented are `assert-equal`, `assert-true` and `assert-raises`. This is enough for me. Additional assertions can be implemented as long as they signal failure by condition derived from `&test-failure`.

## Examples 

See [examples](./examples).

Running: 

```
akku install
scheme examples/test-set-functions.scm
```
