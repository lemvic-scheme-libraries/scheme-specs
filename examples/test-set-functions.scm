;;;;
;;;; Example of a simple set library tests.
;;;;

(library (set-test (0 1 0))
  (export set-tests)
  (import (rnrs)
          (testing))

  ;; Simple set record type. Box for a list reference.
  (define-record-type set
    (nongenerative)
    (fields (mutable elements)))

  ;; Adds an element to the set.
  (define (set-add! s el)
    (let ([elems (set-elements s)])
      (unless (member el elems)
        (set-elements-set! s (cons el elems)))
      s))

  ;; Returns power of the set.
  (define (set-power s)
    (length (set-elements s)))

  ;; Testing routine.
  (define (set-tests)
    (define-specs set-specs
      (it "Test work even at top-level"
        (assert-true #t))
      (describe "Set represented as a list"
        (define (make-empty-set)
          (make-set (list)))
        (let ([s (make-empty-set)])
          (it "is empty by default"
            (define (get-set-power)
              (set-power s))
            (assert-equals 0 (get-set-power)))
          (context "when a new element is added"
            (set-add! s 1)
            (it "increases set power if element did not exist"
              (assert-equals 1 (set-power s)))
            (it "<tests independence> increases set power if element did not exist"
              (set-add! s 2)
              (assert-equals 2 (set-power s)))
            (it "<example of test failure> fails to increase set power if element did not exist"
              (assert-equals 0 (set-power s)))
            (it "<example of exception failure> unexpectedly throws"
              (error 'me "Example runtime error" 'none))))))
    (set-specs)))

(top-level-program (import (set-test)
                           (rnrs)
                           (srfi :39)
                           (testing)
                           (only (testing output)
                                 colored-output
                                 output-successes))
  (define-specs empty-specs)

  (define-specs empty-description-specs
    (describe "Woe is me, there is nothing to describe.")
    (context "I share your pain, `describe`"))

  (parameterize ([colored-output #t]
                 [output-successes #t])
    
    (output-test-summary (run-tests (append (set-tests)
                                            (empty-specs)
                                            (empty-description-specs)))
                         (current-error-port)))
  (exit))
