;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright 2022 Victor Lemeshev <victor.lemeshev@protonmail.com>
;; SPDX-License-Identifier: Apache-2.0
#!r6rs

;;;;
;;;; RSpec and Jasmine inspired testing library for Scheme.
;;;;

(library (testing (0 1 0))
  (export assert-equals
          assert-true
          assert-raises
          define-specs
          run-tests
          output-test-summary)
  (import (rnrs (6))
          (testing compat)
          (testing assertions)
          (testing context)
          (testing output))

  ;; Runs given list of tests and returns a summary.
  (define (run-tests tests)
    (define (call t) (t))
    (map call tests))

  ;; Macro that wraps testing assertions and returns either test-success or &test-failure result.
  (define-syntax test-case
    (syntax-rules ()
      [(_ context forms ...)
       (guard (e [(test-failure? e)
                  (condition e (add-context context))]
                 [(or (error? e) (violation? e))
                  (condition (make-test-error-failure '(forms ...) e) (add-context context))])
             (let ()
               forms ...
               (condition (make-test-success) (add-context context))))]))

  ;; Helper macro for `(it "test case" forms ...)` that collects context and controls the
  ;; execution flow via labels.
  (define-syntax it-helper
    (lambda (x)
      (syntax-case x ()
        [(ih tests labels context cont name forms ...)
         (with-syntax ([label (datum->syntax #'ih (gensym))])
           #'(unless (member 'label labels)
               (set! labels (cons 'label labels))
               (let ([context (reverse (cons name context))])
                 (set! tests (cons (lambda () (test-case context forms ...)) tests))
                 (cont))))])))

  ;; Helper macro for `(describe "description" forms ...)` and `(context "context" forms ...)`
  ;; that collects test context.
  (define-syntax describe-context-helper
    (lambda (x)
      (syntax-case x ()
        [(dch labels context name forms ...)
         (with-syntax ([label (datum->syntax #'dch (gensym))])
           #'(unless (member 'label labels)
               (fluid-let ([context (cons name context)])
                 (let ()
                   forms ...
                   (if #f #f)) ; To support empty `describe` and `context` sections.
                 (set! labels (cons 'label labels)))))])))

  ;; Macro that allows definition of specs of the RSpec kind:
  ;;
  ;; ```
  ;; (define-specs buffer-specs
  ;;   (describe "buffer"
  ;;     (context "when created"
  ;;       (let ([buf (create-buffer)])
  ;;         (it "is empty"
  ;;           (assert-equal? (buffer-size buf) 0)))))))
  ;; ```
  ;;
  ;; Defined `buffer-specs` is a function that returns a list of test cases
  ;; that can be run via `run-tests` invocation.
  (define-syntax define-specs
    (lambda (x)
      (syntax-case x ()
        [(ds name forms ...)
         (with-syntax ([it (datum->syntax #'ds 'it)]
                       [describe (datum->syntax #'ds 'describe)]
                       [context (datum->syntax #'ds 'context)])
           #'(define (name)
               (let ([tests (list)]
                     [seen-labels (list)]
                     [test-context (list)]
                     [specs-continuation #f])
                 (let-syntax ([it (syntax-rules ()
                                    [(_ it-name it-forms (... ...))
                                     (it-helper tests
                                                seen-labels
                                                test-context
                                                specs-continuation
                                                it-name
                                                it-forms (... ...))])]
                              [describe (syntax-rules ()
                                          [(_ d-name d-forms (... ...))
                                           (describe-context-helper seen-labels
                                                                    test-context
                                                                    d-name
                                                                    d-forms
                                                                    (... ...))])]
                              [context (syntax-rules ()
                                         [(_ c-name c-forms (... ...))
                                          (describe-context-helper seen-labels
                                                                   test-context
                                                                   c-name
                                                                   c-forms
                                                                   (... ...))])])
                   (call/cc (lambda (c)
                              (set! specs-continuation c)))
                   (let ()
                     forms ...
                     (if #f #f)) ; This is to support empty specs.
                   tests))))]))))
