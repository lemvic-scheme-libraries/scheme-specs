;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright 2022 Victor Lemeshev <victor.lemeshev@protonmail.com>
;; SPDX-License-Identifier: Apache-2.0
#!r6rs

;;;;
;;;; Tests output facilities.
;;;;

(library (testing output)
  (export colored-output
          output-successes
          output-test-summary)
  (import (rnrs (6))
          (only (srfi :39) make-parameter)
          (only (srfi :48) format)
          (testing context)
          (testing assertions)
          (testing compat))

  ;; Check for boolean parameters.
  (define (ensure-boolean name)
    (lambda (val)
      (unless (boolean? val)
        (error name
               "Value of the parameter must be boolean"))
      val))

  ;; Parameter controlling the possibility of using color control sequences.
  (define colored-output
    (make-parameter #f (ensure-boolean 'colored-output)))

  ;; Parameter controlling the output of successful outcomes.
  (define output-successes
    (make-parameter #f (ensure-boolean 'output-successes)))

  ;; Returns shell escape for colored output.
  (define (color-control color)
    (format #f "\x1b;[1;~am" (case color
                               [(green) 32]
                               [(red) 31]
                               [else 30])))

  ;; Resets the output coloring.
  (define color-reset "\x1b;[0m")

  ;; Unicode bullet point.
  (define bullet-point #\x2022)

  ;; Runs thunk with output produced by it colored.
  (define-syntax with-colored-output
    (syntax-rules ()
      [(_ (color port) body ...)
       (let ([p port]
             [c color])
         (if (colored-output)
             (dynamic-wind
               (lambda () (display (color-control c) port))
               (lambda () body ...)
               (lambda () (display color-reset port)))
             (let () body ...)))]))

  ;; Formats string so that it has color control escapes.
  (define (colored-string color value)
    (cond [(colored-output)
           (format #f "~a~a~a" (color-control color) value color-reset)]
          [(string? value) value]
          [else
           (format #f "~a" value)]))

  ;; Returns color of the output for the outcome.
  (define (outcome-color outcome)
    (if (test-success? outcome)
        'green
        'red))

  ;; Outputs tests summary to a given port.
  (define (output-test-summary cases port)

    (define (output-grouped-results results)
      (define (output-group group depth)
        (cond [(test-outcome? group)
               (format port ": ")
               (with-colored-output ((outcome-color group) port)
                 (output-test-outcome group port))]
              [else
               (for-each (lambda (entry)
                           (let ([description (car entry)]
                                 [sub-group (cadr entry)])
                             (format port "~&~a~a ~a"
                                     (make-string (* 2 depth) #\space)
                                     bullet-point
                                     description)
                             (output-group sub-group (+ depth 1))))
                         group)]))
      (output-group results 0))

    (assert (for-all test-outcome? cases))
    (assert (textual-port? port))

    (let* ([failures (filter test-failure? cases)]
           [failures-count (length failures)]
           [successes (filter test-success? cases)]
           [successes-count (length successes)]
           [outcomes-to-display (if (output-successes)
                                    cases
                                    failures)])
      (output-grouped-results (group-contexts outcomes-to-display))
      (format port
              "~&Tests summary:~%~tSuccesses:~t~a~%~tFailures:~t~a~%"
              (colored-string 'green successes-count)
              (colored-string 'red failures-count))
      (newline port))))
