;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright 2022 Victor Lemeshev <victor.lemeshev@protonmail.com>
;; SPDX-License-Identifier: Apache-2.0
#!r6rs

;;;;
;;;; Chez-Scheme compat procedures.
;;;; This file just re-exports procedures found in ChezScheme.
;;;;

(library (testing compat)
  (export gensym
          display-condition
          iota
          fluid-let)
  (import (chezscheme)))
