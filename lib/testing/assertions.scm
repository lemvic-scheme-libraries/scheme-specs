;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright 2022 Victor Lemeshev <victor.lemeshev@protonmail.com>
;; SPDX-License-Identifier: Apache-2.0
#!r6rs

;;;;
;;;; Library containing supported assertions forms.
;;;;

(library (testing assertions)
  (export output-test-outcome

          &test-failure

          test-failure?
          test-success?
          test-outcome?

          make-test-success
          make-test-failure
          make-test-error-failure

          assert-raises
          assert-equals
          assert-true

          define-failure-condition)
  (import (rnrs (6))
          (only (srfi :48) format)
          (srfi :26)
          (testing compat))

  ;; Root condition for all test results (success and failures)
  (define-condition-type &test-outcome &condition make-test-outcome test-outcome?
    (formatter test-outcome-formatter))

  ;; Test success condition.
  (define-condition-type &test-success &test-outcome %make-test-success test-success?)

  ;; Outputs test success.
  (define (output-test-success success port)
    (display "pass" port))

  ;; Constructor for test success that fixes the display procedure.
  (define make-test-success (cut %make-test-success output-test-success))

  ;; Test failure condition.
  (define-condition-type &test-failure &test-outcome make-test-failure test-failure?
    (form test-failure-form))

  ;; Defines failure condition, providing a function that raises this condition.
  ;; Defined condition has output procedure defined for it.
  (define-syntax define-failure-condition
    (lambda (x)
      (define (generate-definition condition-name output-format fields)
        (define (create-name fmt . parts)
          (define (as-symbol val)
            (cond [(symbol? val) val]
                  [else (syntax->datum val)]))
          (let* ([parts (map as-symbol parts)]
                 [name-symbol (string->symbol (apply format #f fmt parts))])
            (datum->syntax condition-name name-symbol)))

        (define (field-name field)
          (syntax-case field ()
            [(name _ ...) #'name]
            [name #'name]))

        (define (field-formatter field)
          (syntax-case field ()
            [(_ formatter) #'formatter]
            [_ #f]))

        (define (field-accessor field)
          (create-name "~a-~a" condition-name (field-name field)))

        (define (field-output field)
          (let ([accessor (field-accessor field)]
                [formatter (field-formatter field)])
            (if formatter
                #`(lambda (failure) (#,formatter (#,accessor failure)))
                accessor)))

        (define field-definitions (map (lambda (field)
                                         (with-syntax ([name (field-name field)]
                                                       [accessor (field-accessor field)])
                                           #'(name accessor)))
                                       fields))

        (with-syntax ([raise-procedure (create-name "raise-~a" condition-name)]
                      [private-constructor (create-name "%make-~a" condition-name)]
                      [constructor (create-name "make-~a" condition-name)]
                      [predicate (create-name "~a?" condition-name)]
                      [output-procedure (create-name "output-~a" condition-name)]
                      [(f-name ...) (map field-name fields)]
                      [(f-definition ...) field-definitions]
                      [(f-output ...) (map field-output fields)]
                      [output-format output-format]
                      [condition-name (create-name "&~a" condition-name)])

          #'(begin
              (define constructor
                (let ()
                  (define-condition-type condition-name &test-failure private-constructor predicate
                    f-definition ...)
                  (define (output-procedure failure port)
                    (format port output-format
                            (test-failure-form failure)
                            (f-output failure) ...))
                  (cute private-constructor output-procedure <...>)))
              (define (raise-procedure form f-name ...)
                (raise (constructor form f-name ...))))))

      (syntax-case x ()
        [(_ name fmt fields ...)
         (generate-definition #'name #'fmt #'(fields ...))])))

  ;; Comparison failure condition.
  (define-failure-condition test-equality-failure
    "failure, form ~s evaluated to ~s, but ~s was expected (compared via ~s)"
    actual expected comparison)

  ;; Test assertion failure.
  (define-failure-condition test-assertion-failure
    "failure, form ~s evaluated to ~s"
    actual)

  ;; Test failed to raise required condition.
  (define-failure-condition test-raise-failure
    "failure, form ~s evaluated to ~s, but condition matching ~s was expected"
    actual predicate)

  ;; Stringifies condition so that we can pass it into format string.
  (define (stringify-condition cd)
    (call-with-string-output-port (cut display-condition cd <>)))

  ;; Test raised unexpected exception.
  (define-failure-condition test-error-failure
    "failure, form ~s caused condition ~s to be raised"
    (condition stringify-condition))

  ;; Outputs test outcome.
  (define (output-test-outcome outcome port)
    (let ([formatter (test-outcome-formatter outcome)])
      (formatter outcome port)))

  ;; Checks that given expression evaluates to expected-expr and raises
  ;; &check-failure condition otherwise.
  (define-syntax assert-equals
    (syntax-rules ()
      [(_ expected expr)
       (assert-equals expected expr equal?)]
      [(_ expected expr comparison)
       (let ([val expr]
             [exp expected]
             [cmp comparison])
         (unless (cmp val exp)
           (raise-test-equality-failure 'expr val exp 'comparison)))]))

  ;; Checks that given expression evaluates to expected-expr and raises
  ;; &check-failure condition otherwise.
  (define-syntax assert-true
    (syntax-rules ()
      [(_ expr)
       (let ([value expr])
         (unless (and (boolean? value) value)
           (raise-test-assertion-failure 'expr value)))]))

  ;; Checks that given form raises correct condition matched by given predicate.
  (define-syntax assert-raises
    (syntax-rules ()
      [(_ pred? expr)
       (guard (err [(pred? err) #t])
         (let ([val expr])
           (raise-test-raise-failure 'expr val 'pred?)))])))
