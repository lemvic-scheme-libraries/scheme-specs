;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright 2022 Victor Lemeshev <victor.lemeshev@protonmail.com>
;; SPDX-License-Identifier: Apache-2.0
#!r6rs

;;;;
;;;; Compatibility facilities for testing library.
;;;;

(library (testing compat)
  (export iota
          display-condition
          gensym
          fluid-let)
  (import (rnrs (6))
          (only (srfi :48) format))

  (define (display-condition cond port)
    (format port "~a" cond))

  (define (iota len)
    (reverse (let loop ([index 0]
                        [result (list)])
               (if (>= index len)
                   result
                   (loop (+ index 1)
                         (cons index result))))))

  (define (gensym)
    (car (generate-temporaries (list #f))))

  (define-syntax fluid-let
    (syntax-rules ()
      [(_ ([b e] ...) body ...)
       (let ([val e] ...)
         (define (exchange)
           (let ([temp b])
             (set! b val)
             (set! val temp))
           ...)
         (dynamic-wind exchange
                       (lambda () body ...)
                       exchange))]
      [(_ () body ...)
       (let () body ...)])))
