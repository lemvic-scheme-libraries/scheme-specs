;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright 2022 Victor Lemeshev <victor.lemeshev@protonmail.com>
;; SPDX-License-Identifier: Apache-2.0
#!r6rs

;;;;
;;;; Context utilities (grouping basically).
;;;;

(library (testing context)
  (export group-contexts
          (rename (make-test-case-context add-context))
          test-case-context)
  (import (rnrs (6))
          (only (srfi :48) format)
          (srfi :26)
          (testing assertions))

  ;; Condition encapsulating text case context.
  ;; This condition gets added to test successes or failures upon construction.
  (define-condition-type &test-case-context &condition make-test-case-context test-case-context?
    (context test-case-context))

  ;; Converts a list of lists (contexts) of the form ("string-a" ... "string-n" non-string-leaf)
  ;; into a tree where branches correspond to prefixes.
  ;; Example:
  ;; (define example '(("a" "b" "d" 1)
  ;;                   ("a" "b" "c" "e" "f" 2)
  ;;                   ("a" "b" "c" "e" "f" 3)
  ;;                   ("a" "b" "c" 4)))
  ;; (group example) ->
  ;; (("a"
  ;;   (("b"
  ;;     (("c"
  ;;       (("e"
  ;;         (("f" 3 2))))
  ;;       4)
  ;;      ("d" 1))))))
  (define (group lists)
    (define (collect lists entries)
      (define (add-entry result current)
        (let* ([key (car current)]
               [entry (hashtable-ref result key (list))])
          (hashtable-set! result key (cons (cdr current) entry))
          result))

      (hashtable-clear! entries)

      (let ([entries* (fold-left add-entry entries lists)]
            [result (list)])
        (let-values ([(keys values) (hashtable-entries entries)])
          (vector-for-each (lambda (key value)
                             (define flatten-1 (cut map car <...>))
                             (define (subgroup? value)
                               (string? (car value)))
                             (let-values ([(subgroups leafs) (partition subgroup? value)])
                               (if (null? subgroups)
                                   (set! result (cons (cons key (flatten-1 leafs)) result))
                                   (let ([subgroups (collect subgroups entries)])
                                     (set! result
                                           (cons (cons* key subgroups (flatten-1 leafs))
                                                 result))))))
                           keys values)
          result)))

    (collect lists (make-hashtable equal-hash equal?)))

  ;; Groups contexts of independent tests together to form a tree.
  (define (group-contexts results)
    (define (extract-contexts result)
      (append (test-case-context result) (list result)))

    (assert (for-all test-outcome? results))
    (assert (for-all test-case-context? results))

    (let ([res (map extract-contexts results)])
      (group res))))
